Name:             adobe-mappings-cmap
Summary:          CMap resources for Adobe's character collections
Version:          20231115
Release:          1
License:          BSD

URL:              https://www.adobe.com/
Source:           https://github.com/adobe-type-tools/cmap-resources/archive/%{version}.tar.gz

BuildArch:        noarch
Requires:         %{name}-lang = %{version}-%{release}

%description
This open source project provides the latest CMap resources for Adobe’s public character collections.
CMap (Character Map) resources are used to unidirectionally map character codes, 
such as a Unicode encoding form, to CIDs (Characters IDs, meaning glyphs) of a CIDFont resource. 

%package          devel
Summary:          RPM macros for Adobe's CMap resources for character collections
Requires:         %{name} = %{version}-%{release} 

%description      devel
This package installs RPM macros useful for building packages against adobe-mappings-cmap,as well as all the fonts contained in this font set.

%package_help

%package          lang
Summary:          RPM macros for Adobe's CMap resources for character collectionsa
Requires:         %{name} = %{version}-%{release}
provides:         %{name}-deprecated
Obsoletes:        %{name}-deprecated

%description      lang
This package provides localization support such as language and time zone.

%prep
%autosetup -n cmap-resources-%{version} 

%install
%make_install prefix=%{_prefix}

install -m 0755 -d %{buildroot}%{_rpmconfigdir}/macros.d

cat > %{buildroot}%{_rpmconfigdir}/macros.d/macros.%{name} << _EOF
%%adobe_mappings_rootpath     %{_datadir}/adobe/resources/mapping/
_EOF

%files
%license LICENSE.md
%doc  README.md  VERSIONS.txt

%files devel
%{_rpmmacrodir}/macros.adobe-mappings-cmap

%files help
%doc  README.md  VERSIONS.txt

%files lang
%dir %{_datadir}/adobe
%dir %{_datadir}/adobe/resources
%dir %{_datadir}/adobe/resources/mapping
%{_datadir}/adobe/resources/mapping/CNS1
%{_datadir}/adobe/resources/mapping/KR
%{_datadir}/adobe/resources/mapping/GB1
%{_datadir}/adobe/resources/mapping/Korea1
%{_datadir}/adobe/resources/mapping/Japan1
%{_datadir}/adobe/resources/mapping/deprecated
%{_datadir}/adobe/resources/mapping/Identity
%{_datadir}/adobe/resources/mapping/Manga1

%changelog
* Wed Jan 24 2024 zhangpan <zhangpan103@h-partners.com> - 20231115-1
- update to 20231115

* Wed Sep 9 2020 hanhui <hanhui15@huawei.com> - 20190730-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source url

* Wed Sep 4 2019 openEuler Buildteam <buildteam@openeuler.org> -20190730-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:Increase the ability to provide adobe-mappings-cmap-deprecated

* Tue Sep 3 2019 openEuler Buildteam <buildteam@openeuler.org> -20190730-1
- Package init
